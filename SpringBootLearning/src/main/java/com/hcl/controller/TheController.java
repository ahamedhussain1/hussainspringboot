package com.hcl.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TheController {
	
	//localhost:8081/api
	@GetMapping()
	public String firstMethod() {
		return "Hello World";
	}
	
	//localhost:8081/api/theId?id=123
	@GetMapping("/theId")
	public String getId( @RequestParam String id) {
		return "Id :" + id;
	}
	
	//localhost:8081/api/details?id=1&name=hussain
	@PostMapping("/details")
	public String addDetails(@RequestParam(name = "id") String theId, @RequestParam String name) { 
	    return "ID: " + theId + " Name: " + name;
	}
	
	//localhost:8081/api/url?id=123
	@GetMapping("/url")
	public String getIds(@RequestParam(required = false) String id) { 
	    return "ID: " + id;
	}
	
	//localhost:8081/api/byPathariable/1
	@GetMapping("/byPathariable/{id}")
	public String getFooByOptionalId(@PathVariable(required = false) String id){
	    return "ID: " + id;
	}
	
	
	@PostMapping
	public String paramExample (
			@RequestParam("name") String name,
			@RequestParam("age") int age,
			@RequestParam(value = "admin", defaultValue = "false")boolean isAdmin,
			@RequestBody Map<String,Object> obj){   
		return "Name : "+name +"\nage : "+age+"\nand admin status is : "+isAdmin+"\n"+obj.toString();
	}
	
	@PostMapping("/example/{name}/{id}/{isAdmin}")
	public String PathExample(
			@PathVariable("name") String name,
			@PathVariable("id") int id,  
			@PathVariable("isAdmin") boolean isAdmin,             
			@RequestBody Map<String,Object> obj){    
		return "Name : "+name +"\nid : "+id+"\nand admin status is : "+isAdmin+"\n"+obj.toString();
	}
	

}
